/**
 * NameController
 *
 * @description :: Server-side logic for managing names
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	'PageList':function(req,res){
		/*Name.findOne(req.param('id'),function FoundList(err,name){
			if(err) return res.json(err);
			if(!name)return res.json(name);
		});*/
		Name.native(function(err,coll){
  /*coll.distinct("_id", function(err,result){
     res.json(result);*/
       coll.find({}, {_id: true,pageUrl:true}).toArray(function (err, results) {
    	  if (err) return res.serverError(err);
    	  return res.ok(results);
    });
	});
}
};

